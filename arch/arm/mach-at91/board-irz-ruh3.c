/*
 * linux/arch/arm/mach-at91/board-rm9200dk.c
 *
 *  Copyright (C) 2005 SAN People
 *
 *  Epson S1D framebuffer glue code is:
 *     Copyright (C) 2005 Thibaut VARENE <varenet@parisc-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/types.h>
#include <linux/gpio.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/mtd/physmap.h>
#include <linux/gpio-lv.h>

#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/irq.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>

#include <mach/hardware.h>
#include <mach/board.h>
#include <mach/at91rm9200_mc.h>

#include "generic.h"


static void __init irz_ruh3_init_early(void)
{
	/* Initialize processor: 18.432 MHz crystal */
	at91_initialize(18432000);
}

static struct macb_platform_data __initdata irz_ruh3_eth_data = {
	.phy_irq_pin	= -1,
	.is_rmii	= 0,
};

static struct at91_usbh_data __initdata irz_ruh3_usbh_data = {
	.ports		= 2,
	.vbus_pin	= {-EINVAL, -EINVAL},
	.overcurrent_pin= {-EINVAL, -EINVAL},
};

static struct i2c_board_info __initdata irz_ruh3_i2c_devices[] = {
	{
		I2C_BOARD_INFO("m41t00", 0x68),
	},
};

#define IRZ_RXX_FLASH_BASE	AT91_CHIPSELECT_0
#define IRZ_RXX_FLASH_SIZE	SZ_8M

static struct physmap_flash_data irz_ruh3_flash_data = {
	.width		= 2,
};

static struct resource irz_ruh3_flash_resource = {
	.start		= IRZ_RXX_FLASH_BASE,
	.end		= IRZ_RXX_FLASH_BASE + IRZ_RXX_FLASH_SIZE - 1,
	.flags		= IORESOURCE_MEM,
};

static struct platform_device irz_ruh3_flash = {
	.name		= "physmap-flash",
	.id		= 0,
	.dev		= {
				.platform_data	= &irz_ruh3_flash_data,
			},
	.resource	= &irz_ruh3_flash_resource,
	.num_resources	= 1,
};


static struct at91_mmc_data __initdata irz_ruh3_mmc_data = {
	.det_pin	= AT91_PIN_PB7,
	.slot_b		= 0,
	.wire4		= 0,
	.wp_pin		= -1,
	.vcc_pin	= -1
};

static struct gpio_led irz_ruh3_leds[] = {
	{
		.name			= "SIM1",
		.gpio			= AT91_PIN_PB6,
		.active_low		= 0,
		.default_trigger= "none",
	},
	{
		.name			= "SIM2",
		.gpio			= AT91_PIN_PD0,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "3G",
		.gpio			= AT91_PIN_PB29,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "CSQ3",
		.gpio			= AT91_PIN_PC10,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "CSQ2",
		.gpio			= AT91_PIN_PC11,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "CSQ1",
		.gpio			= AT91_PIN_PC12,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "CSQ0",
		.gpio			= AT91_PIN_PC13,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "PPP",
		.gpio			= AT91_PIN_PD18,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "BTN_I",
		.gpio			= AT91_PIN_PC2,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "USER1",
		.gpio			= AT91_PIN_PD15,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "USER2",
		.gpio			= AT91_PIN_PD16,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "USER3",
		.gpio			= AT91_PIN_PD17,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "POWER",
		.gpio			= AT91_PIN_PD26,
		.active_low		= 0,
		.default_state  = LEDS_GPIO_DEFSTATE_ON,
		.default_trigger	= "timer",
	},
};

#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)
static struct gpio_keys_button irz_ruh3_buttons[] = {
	{
		.gpio		= AT91_PIN_PB28,
		.code		= BTN_0,
		.desc		= "Internet",
		.active_low	= 1,
		.wakeup		= 1,
	},
	{
		.gpio		= AT91_PIN_PD14,
		.code		= BTN_1,
		.desc		= "Reset button",
		.active_low	= 1,
		.wakeup		= 1,
	},
};

static struct gpio_keys_platform_data irz_ruh3_button_data = {
	.buttons	= irz_ruh3_buttons,
	.nbuttons	= ARRAY_SIZE(irz_ruh3_buttons),
};

static struct platform_device irz_ruh3_button_device = {
	.name		= "gpio-keys",
	.id		= -1,
	.num_resources	= 0,
	.dev		= {
		.platform_data	= &irz_ruh3_button_data,
	}
};

static void __init irz_ruh3_add_device_buttons(void)
{
	at91_set_gpio_input(AT91_PIN_PB28, 1);	/* Internet button */
	at91_set_deglitch(AT91_PIN_PB28, 1);

	at91_set_gpio_input(AT91_PIN_PD14, 1);	/* Reset button */
	at91_set_deglitch(AT91_PIN_PD14, 1);

	platform_device_register(&irz_ruh3_button_device);
}
#else
static void __init irz_ruh3_add_device_buttons(void) {}
#endif

#ifdef CONFIG_GPIO_LV

static struct gpio_lv_pin irz_ruh3_lv_pins[] = {
    {
	.input_pin 	= AT91_PIN_PD4,
	.direction_pin	= AT91_PIN_PD3,
	.name		= "IO_1",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD6,
	.direction_pin	= AT91_PIN_PD5,
	.name		= "IO_2",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD8,
	.direction_pin	= AT91_PIN_PD7,
	.name		= "IO_3",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD10,
	.direction_pin	= AT91_PIN_PD9,
	.name		= "IO_4",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD12,
	.direction_pin	= AT91_PIN_PD11,
	.name		= "IO_5",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD20,
	.direction_pin	= AT91_PIN_PD19,
	.name		= "IO_6",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD22,
	.direction_pin	= AT91_PIN_PD21,
	.name		= "IO_7",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PD27,
	.direction_pin	= AT91_PIN_PD23,
	.name		= "IO_8",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
    {
	.input_pin 	= AT91_PIN_PC15,
	.direction_pin	= AT91_PIN_PC14,
	.name		= "IO_9",
	.flags		= GPIOF_IN|GPIOF_EXPORT_DIR_CHANGEABLE,
    },
};

static struct gpio_lv_platform_data irz_ruh3_gpio_lv_data = {
	.pins  = irz_ruh3_lv_pins,
	.npins = ARRAY_SIZE(irz_ruh3_lv_pins),
};

static struct platform_device irz_ruh3_gpio_lv_device = {
	.name		= "gpio-lv",
	.id		= -1,
	.num_resources	= 0,
	.dev		= {
		.platform_data	= &irz_ruh3_gpio_lv_data,
	}
};

#endif

static struct gpio irz_ruh3_other_gpios[] = {
		{
			.gpio  = AT91_PIN_PB10,
			.label = "BUZZER",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PC0,
			.label = "RS485_PRESENSE",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PA4,
			.label = "GSM_POWER",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PA24,
			.label = "SIM_SELECT",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PB9,
			.label = "SIM1_PRES",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PB11,
			.label = "SIM2_PRES",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PB2,
			.label = "RE",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PC8,
			.label = "USB_POWER",
			.flags = GPIOF_OUT_INIT_HIGH
		},
/*
		{
			.gpio  = AT91_PIN_PD1,
			.label = "DIR_IO_1",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD2,
			.label = "IO_1",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD3,
			.label = "DIR_IO_2",
			.flags = GPIOF_OUT_INIT_LOW
		},

		{
			.gpio  = AT91_PIN_PD4,
			.label = "IO_2",
			.flags = GPIOF_IN
		},

		{
			.gpio  = AT91_PIN_PD5,
			.label = "DIR_IO_3",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD6,
			.label = "IO_3",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD7,
			.label = "DIR_IO_4",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD8,
			.label = "IO_4",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD9,
			.label = "DIR_IO_5",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD10,
			.label = "IO_5",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD11,
			.label = "DIR_IO_6",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD12,
			.label = "IO_6",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD19,
			.label = "DIR_IO_7",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD20,
			.label = "IO_7",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD21,
			.label = "DIR_IO_8",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD22,
			.label = "IO_8",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PD23,
			.label = "DIR_IO_9",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PD27,
			.label = "IO_9",
			.flags = GPIOF_IN
		},
		{
			.gpio  = AT91_PIN_PC14,
			.label = "DIR_IO_10",
			.flags = GPIOF_OUT_INIT_LOW
		},
		{
			.gpio  = AT91_PIN_PC15,
			.label = "IO_10",
			.flags = GPIOF_IN
		},
		*/
};

static void __init irz_ruh3_add_gpios(struct gpio gpios[], int size){
	int i = 0;
	//struct device *dev = ;

	for( i=0; i < size; i++){
		struct gpio item = gpios[i];
		at91_set_GPIO_periph(item.gpio, 0);
		gpio_request_one(item.gpio, item.flags, item.label);
		gpio_export(item.gpio, 1);
	}
}


static void __init irz_ruh3_board_init(void)
{
	/* Setup the LEDs */
	//at91_init_leds(AT91_PIN_PB2, AT91_PIN_PB2);

	/* DBGU on ttyS0. (Rx & Tx only) */
	at91_register_uart(0, 0, 0);

	at91_register_uart(AT91RM9200_ID_US0, 1, 0);
	/* USART1 on ttyS1. (Rx, Tx, CTS, RTS, DTR, DSR, DCD, RI) */
	at91_register_uart(AT91RM9200_ID_US1, 2, ATMEL_UART_CTS | ATMEL_UART_RTS
			   | ATMEL_UART_DTR | ATMEL_UART_DSR | ATMEL_UART_DCD
			   | ATMEL_UART_RI| ATMEL_UART_RS485);
	at91_register_uart(AT91RM9200_ID_US2, 4, 0);
	at91_register_uart(AT91RM9200_ID_US3, 3, ATMEL_UART_CTS | ATMEL_UART_RTS
			   | ATMEL_UART_DTR | ATMEL_UART_DSR | ATMEL_UART_DCD);

	/* set serial console to ttyS0 (ie, DBGU) */
	//at91_set_serial_console(0);

	/* Serial */
	at91_add_device_serial();
	/* Ethernet */
	at91_add_device_eth(&irz_ruh3_eth_data);
	/* USB Host */
	at91_add_device_usbh(&irz_ruh3_usbh_data);
	/* I2C */
	at91_add_device_i2c(irz_ruh3_i2c_devices, ARRAY_SIZE(irz_ruh3_i2c_devices));
	/* NOR Flash */
	platform_device_register(&irz_ruh3_flash);
	/* MMC Host */
	at91_add_device_mmc(0, &irz_ruh3_mmc_data);
	/* LEDs  */
	at91_gpio_leds(irz_ruh3_leds, ARRAY_SIZE(irz_ruh3_leds));
	/* Push Buttons */
	irz_ruh3_add_device_buttons();
#ifdef CONFIG_GPIO_LV
	/* Levelshifted gpio*/
	platform_device_register(&irz_ruh3_gpio_lv_device);
#endif
	/*export other gpios*/
	irz_ruh3_add_gpios(irz_ruh3_other_gpios, ARRAY_SIZE(irz_ruh3_other_gpios));
}

MACHINE_START(IRZ_RXX, "IRZ RUH3 router board")
	/* Maintainer: Radiofid */
	.init_early		= irz_ruh3_init_early,
	.timer			= &at91rm9200_timer,
	.map_io			= at91_map_io,
	.init_irq		= at91_init_irq_default,
	.init_machine		= irz_ruh3_board_init,
MACHINE_END
