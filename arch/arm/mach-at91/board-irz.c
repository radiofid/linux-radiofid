/*
 * linux/arch/arm/mach-at91/board-rm9200dk.c
 *
 *  Copyright (C) 2005 SAN People
 *
 *  Epson S1D framebuffer glue code is:
 *     Copyright (C) 2005 Thibaut VARENE <varenet@parisc-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/types.h>
#include <linux/gpio.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/mtd/physmap.h>

#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/irq.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>

#include <mach/hardware.h>
#include <mach/board.h>
#include <mach/at91rm9200_mc.h>

#include "generic.h"


static void __init irz_rxx_init_early(void)
{
	/* Initialize processor: 18.432 MHz crystal */
	at91_initialize(18432000);
}

static struct macb_platform_data __initdata irz_rxx_eth_data = {
	.phy_irq_pin	= -EINVAL,
	.is_rmii	= 0,
};

static struct at91_usbh_data __initdata irz_rxx_usbh_data = {
	.ports		= 2,
	.vbus_pin	= {-EINVAL, -EINVAL},
	.overcurrent_pin= {-EINVAL, -EINVAL},
	.overcurrent_supported = 0,
};

static struct i2c_board_info __initdata irz_rxx_i2c_devices[] = {
	{
		I2C_BOARD_INFO("m41t00", 0x68),
	},
};

#define IRZ_RXX_FLASH_BASE	AT91_CHIPSELECT_0
#define IRZ_RXX_FLASH_SIZE	SZ_8M

static struct physmap_flash_data irz_rxx_flash_data = {
	.width		= 2,
};

static struct resource irz_rxx_flash_resource = {
	.start		= IRZ_RXX_FLASH_BASE,
	.end		= IRZ_RXX_FLASH_BASE + IRZ_RXX_FLASH_SIZE - 1,
	.flags		= IORESOURCE_MEM,
};

static struct platform_device irz_rxx_flash = {
	.name		= "physmap-flash",
	.id		= 0,
	.dev		= {
				.platform_data	= &irz_rxx_flash_data,
			},
	.resource	= &irz_rxx_flash_resource,
	.num_resources	= 1,
};

static struct gpio_led irz_rxx_leds[] = {
	{
		.name			= "WLAN_LOW",
		.gpio			= AT91_PIN_PA6,
		.active_low		= 1,
		.default_trigger	= "none",
	},
	{
		.name			= "WLAN_HIGH",
		.gpio			= AT91_PIN_PA4,
		.active_low		= 1,
		.default_trigger	= "none",
	},
	{
		.name			= "CSQ_LOW",
		.gpio			= AT91_PIN_PA2,
		.active_low		= 1,
		.default_trigger	= "none",
	},
	{
		.name			= "CSQ_HIGH",
		.gpio			= AT91_PIN_PA0,
		.active_low		= 1,
		.default_trigger	= "none",
	},
	{
		.name			= "SIM1",
		.gpio			= AT91_PIN_PA29,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "SIM2",
		.gpio			= AT91_PIN_PA27,
		.active_low		= 0,
		.default_trigger	= "none",
	},
	{
		.name			= "BUSY",
		.gpio			= AT91_PIN_PB29,
		.active_low		= 0,
		.default_trigger	= "default-on",
	},
};

#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)
static struct gpio_keys_button irz_rxx_buttons[] = {
	{
		.gpio		= AT91_PIN_PB28,
		.code		= BTN_0,
		.desc		= "Reset button",
		.active_low	= 1,
		.wakeup		= 1,
	},

};

static struct gpio_keys_platform_data irz_rxx_button_data = {
	.buttons	= irz_rxx_buttons,
	.nbuttons	= ARRAY_SIZE(irz_rxx_buttons),
};

static struct platform_device irz_rxx_button_device = {
	.name		= "gpio-keys",
	.id		= -1,
	.num_resources	= 0,
	.dev		= {
		.platform_data	= &irz_rxx_button_data,
	}
};

static void __init irz_rxx_add_device_buttons(void)
{
	at91_set_gpio_input(AT91_PIN_PB28, 1);	/* btn3 */
	at91_set_deglitch(AT91_PIN_PB28, 1);

	platform_device_register(&irz_rxx_button_device);
}
#else
static void __init irz_rxx_add_device_buttons(void) {}
#endif

static struct gpio irz_rxx_other_gpios[] = {

		{
			.gpio		= AT91_PIN_PA3,
			.label		= "GSM_POWER",
			.flags = GPIOF_OUT_INIT_LOW

		},
		{
			.gpio		= AT91_PIN_PA24,
			.label		= "SIM_SELECT",
			.flags = GPIOF_OUT_INIT_HIGH
		},
		{
			.gpio		= AT91_PIN_PB9,
			.label		= "SIM1_PRES",
			.flags = GPIOF_IN
		},
		{
			.gpio		= AT91_PIN_PB11,
			.label		= "SIM2_PRES",
			.flags	= GPIOF_IN
		},
};

static void __init irz_rxx_add_gpios(struct gpio gpios[], int size){
	int i = 0;
	//struct device *dev = ;

	for( i=0; i < size; i++){
		struct gpio item = gpios[i];
		at91_set_GPIO_periph(item.gpio, 0);
		gpio_request_one(item.gpio, item.flags, item.label);
		gpio_export(item.gpio, 1);
	}
}

static void __init irz_rxx_board_init(void)
{
		/* DBGU on ttyS0. (Rx & Tx only) */
	at91_register_uart(0, 0, 0);

	at91_register_uart(AT91RM9200_ID_US0, 1, 0);
	/* USART1 on ttyS1. (Rx, Tx, CTS, RTS, DTR, DSR, DCD, RI) */
	at91_register_uart(AT91RM9200_ID_US1, 2, ATMEL_UART_CTS | ATMEL_UART_RTS
			   | ATMEL_UART_DTR | ATMEL_UART_DSR | ATMEL_UART_DCD
			   | ATMEL_UART_RI);
	at91_register_uart(AT91RM9200_ID_US2, 3, 0);
	at91_register_uart(AT91RM9200_ID_US3, 4, 0);
	/* Serial */
	at91_add_device_serial();
	/* Ethernet */
	at91_add_device_eth(&irz_rxx_eth_data);
	/* USB Host */
	at91_add_device_usbh(&irz_rxx_usbh_data);
	/* I2C */
	at91_add_device_i2c(irz_rxx_i2c_devices, ARRAY_SIZE(irz_rxx_i2c_devices));
	/* NOR Flash */
	platform_device_register(&irz_rxx_flash);
	/* LEDs  */
	at91_gpio_leds(irz_rxx_leds, ARRAY_SIZE(irz_rxx_leds));
	/* Push Buttons */
	irz_rxx_add_device_buttons();
	/*export other gpios*/
	irz_rxx_add_gpios(irz_rxx_other_gpios, ARRAY_SIZE(irz_rxx_other_gpios));
}

MACHINE_START(IRZ_RXX, "IRZ RXX router board")
	/* Maintainer: Radiofid */
//	.boot_params	= AT91_SDRAM_BASE + 0x100,
	.init_early		= irz_rxx_init_early,
	.timer			= &at91rm9200_timer,
	.map_io			= at91_map_io,
	.init_irq		= at91_init_irq_default,
	.init_machine	= irz_rxx_board_init,
MACHINE_END
