/*

Driver for GPIO through levelshifters using two pins from SoC for direction and value

*/
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/gpio-lv.h>

int gpio_lv_get_direction(struct gpio_chip *chip, unsigned offset ){
    struct gpio_lv_platform_data *pdata = (struct gpio_lv_platform_data *)dev_get_platdata(chip->dev);
    dev_info(chip->dev, "gpio_lv get direction called for pin %d", offset); 
    return gpio_get_value( pdata->pins[offset].direction_pin );
}
int gpio_lv_dir_input(struct gpio_chip *chip, unsigned offset ){
    const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->dev);
    
    dev_info(chip->dev, "gpio_lv dir input called for pin %d", offset); 
    gpio_set_value( pdata->pins[offset].direction_pin, 0);
    gpio_direction_input( pdata->pins[offset].input_pin);
    
    return 0;
}
int gpio_lv_dir_output(struct gpio_chip *chip, unsigned offset, int value ){
    const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->dev);
    
    dev_info(chip->dev, "gpio_lv dir output called for pin %d", offset); 
    gpio_set_value( pdata->pins[chip->base-offset].direction_pin, 1);
    gpio_direction_output( pdata->pins[offset].input_pin, value);
    
    return 0;
}
int gpio_lv_get_value(struct gpio_chip *chip, unsigned offset ){
    const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->dev);
    
    dev_info(chip->dev, "gpio_lv get value called for pin %d", offset); 
    return gpio_get_value( pdata->pins[offset].input_pin);    
}
void gpio_lv_set_value(struct gpio_chip *chip, unsigned offset, int value ){
    const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->dev);
    dev_info(chip->dev, "gpio_lv set value called for pin %d", offset); 
    gpio_set_value( pdata->pins[offset].input_pin, value);
    
    return;
}

static int gpio_lv_to_irq( struct gpio_chip *chip, unsigned offset ){
    const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->dev);
    return gpio_to_irq( pdata->pins[offset].input_pin);
}

static int gpio_lv_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct gpio_lv_platform_data *pdata = dev_get_platdata(dev);
	struct gpio_chip* lv_chip;
	int i, ret;
    const char** names;
	if ( !pdata ){
	    dev_err(dev,"Can't get platform data\n");
	    return -EINVAL;
	}
/*	if (!pdata) {
		pdata = gpio_lv_get_devtree_pdata(dev);
		if (IS_ERR(pdata))
			return PTR_ERR(pdata);
	}
*/	
	dev_info(dev, "init with %d gpios", pdata->npins);
    lv_chip = kmalloc( sizeof( struct gpio_chip ), GFP_KERNEL|__GFP_ZERO);
	
	if( !lv_chip ){
	    dev_err(dev,"failed to allocate lv_chip struct\n");
	    return -ENOMEM;
	}
    	
	/* set handlers for gpio operations*/
	lv_chip->direction_input	= gpio_lv_dir_input;
	lv_chip->direction_output	= gpio_lv_dir_output;
	lv_chip->get			= gpio_lv_get_value;
	lv_chip->set			= gpio_lv_set_value;
    lv_chip->to_irq         = gpio_lv_to_irq;
//	lv_chip->get_direction		= gpio_lv_get_direction;
	
	lv_chip->ngpio		= pdata->npins;
	lv_chip->can_sleep	= 1;
	lv_chip->base 		= -1;	/* this is "virtual" gpio chip, so we aren't need to set base num explicitly.*/
	
	lv_chip->dev	= dev;
	lv_chip->owner	= THIS_MODULE;
	names 	        = kmalloc( sizeof(char*) * lv_chip->ngpio, GFP_KERNEL|__GFP_ZERO);

	if( !names ){
	    dev_err(dev,"failed to allocate lv_chip->names array\n");
	    kfree(lv_chip);
	    return -ENOMEM;
	}

    //memset(names, 0, sizeof(char*) * lv_chip->ngpio );

	/* initialize undelying gpios to begin state */
	for( i=0; i < lv_chip->ngpio; i++ ){
	    struct gpio_lv_pin* pin = &(pdata->pins[i]);
	    
	    if( gpio_is_valid(pin->input_pin) == false ){
		dev_err(dev, "Input pin(%d) in %s pin not valid",pin->input_pin, pin->name);
		ret = -EINVAL;
		goto fail2;
	    } 
	    
	    if( gpio_is_valid(pin->direction_pin) == false ){
		dev_err(dev, "Direction pin(%d) in %s pin not valid",pin->direction_pin, pin->name);
		ret = -EINVAL;
		goto fail2;
	    }
	    
	    if( gpio_request(pin->input_pin, NULL) != 0 ){
		dev_err(dev, "Input pin(%d) in %s pin already in use",pin->input_pin, pin->name);
		ret = -EBUSY;
		goto fail2;
	    }
	    if( gpio_request(pin->direction_pin, NULL) != 0 ){
		dev_err(dev, "Direction pin(%d) in %s pin already in use",pin->direction_pin, pin->name);
		ret = -EBUSY;
		goto fail2;
	    }
	    
	    names[i] = pin->name ? pin->name : NULL;
	    
	    gpio_direction_output( pin->direction_pin, 0 );
	    gpio_direction_input( pin->input_pin );
	    
	    if( pin->flags | GPIOF_DIR_IN ){
		    gpio_set_value( pin->direction_pin, 0);
	    } else if( pin->flags | GPIOF_DIR_OUT ){
		    gpio_set_value( pin->direction_pin, 1);
		    gpio_direction_output( pin->input_pin, 0 );
	    }
	    
	    //TODO: may be need to add active low handling
	    if( pin->flags | GPIOF_INIT_LOW ) {
		    gpio_set_value( pin->input_pin, 0);
	    } else if( pin->flags | GPIOF_INIT_HIGH ){
		    gpio_set_value( pin->input_pin, 1);
	    }
	    
        pin->claimed = 1;
	    continue;
fail2:
	    pin->claimed = 0;
	}
	
    lv_chip->names = names;
	/* register gpio chip */
	
	ret = gpiochip_add( lv_chip );
	
	if( ret ){
	    for( i=0; i < pdata->npins; i++ ){
		struct gpio_lv_pin* pin = &(pdata->pins[i]);
	    
		if( pin->claimed ){
		    gpio_free(pin->input_pin);
		    gpio_free(pin->direction_pin);
		    pin->claimed = 0;
		}
	    }	    
	    goto fail;
	}
	
	pdata->chip = lv_chip;
	
    // export pins if needed
	for( i=0; i < pdata->npins; i++ ){
		struct gpio_lv_pin* pin = &(pdata->pins[i]);
	    if( pin->flags | GPIOF_EXPORT ) {

	        if( gpio_request(lv_chip->base + i, NULL) != 0 ){
		        dev_err(dev, "can't claim(%d) pin",lv_chip->base + i);
		        continue;
	        }

            gpio_export(lv_chip->base + i, (pin->flags & GPIOF_EXPORT_CHANGEABLE));
        }
    }

	return ret;
fail:
	kfree( lv_chip->names );
	kfree( lv_chip );
	return ret;
}

static int gpio_lv_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(dev);
	int i;
	
	for( i=0; i < pdata->npins; i++ ){
	    struct gpio_lv_pin* pin = &(pdata->pins[i]);
	    
	    if( pin->claimed ){
		gpio_free(pin->input_pin);
		gpio_free(pin->direction_pin);
		pin->claimed = 0;
	    }
	}
	
	kfree( pdata->chip->names );
	kfree( pdata->chip );
	
	return 0;
}

static struct platform_driver gpio_lv_device_driver = {
	.probe		= gpio_lv_probe,
	.remove		= gpio_lv_remove,
	.driver		= {
		.name	= "gpio-lv",
		.owner	= THIS_MODULE,
//		.pm	= &gpio_lv_pm_ops,
//		.of_match_table = of_match_ptr(gpio_lv_of_match),
	}
};

static int __init gpio_lv_init(void)
{
	return platform_driver_register(&gpio_lv_device_driver);
}

static void __exit gpio_lv_exit(void)
{
	platform_driver_unregister(&gpio_lv_device_driver);
}

late_initcall(gpio_lv_init);
module_exit(gpio_lv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nikita Nazarenko <nnazarenko@radiofid.com>");
MODULE_DESCRIPTION("Wrapper GPIO driver over LV*** levelshifter");
MODULE_ALIAS("platform:gpio-lv");
