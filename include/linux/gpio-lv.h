#ifndef _GPIO_LV_H
#define _GPIO_LV_H

#include <linux/gpio.h>

struct gpio_lv_pin {
	/* Configuration parameters */
	int input_pin; 		/* Input gpio pin number */
	int direction_pin; 	/* Direction gpio pin number */
	int flags;		/* Pin flags*/
	int claimed;		/* Is pin activated*/
//	int active_low;		/* Is pin inverted */
	const char* name;	/* GPIO pin name*/		
};

struct gpio_lv_platform_data {
	struct gpio_lv_pin *pins;
	int npins;
	struct gpio_chip* chip;
};

#endif
